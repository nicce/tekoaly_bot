import datetime
import random
from datetime import time
import requests
from bs4 import BeautifulSoup
from ..utils import finnish_months, daily_wisdoms

from .weather import get_temp
from ..conf.conf import logger

# logger.name = __name__

"""
Get current date. Transform date to month, day, week variables
"""


def get_month():
    date = datetime.datetime.now().date()
    month = finnish_months[date.month - 1]
    return month


def get_day():
    date = datetime.datetime.now().date()
    day = date.day
    return day


def get_week():
    week = datetime.date.today().isocalendar()[1]
    return week


"""
Get name day
"""


def get_name_day(page, url):
    if page > 0:
        code = requests.get(url)
        plain = code.text
        s = BeautifulSoup(plain, "html.parser")
        span = s.find("a", {"class": ""})
        return str(span.get_text())
    else:
        return "name day API connection error!"


"""
Get wisdom
"""


def get_wisdom():
    wisdom = random.choice(daily_wisdoms)
    return wisdom


"""
Concatenate final morning greeting
"""


def get_greeting():
    current_temp, highest_temp, lowest_temp, current_summary = get_temp()
    logger.debug(f"Getting greeter. Temperature args are {current_temp, highest_temp, lowest_temp}")

    greeting = (
        "Hyvää huomenta! Tänään on "
        + get_month()
        + "n "
        + str(get_day())
        + "."
        + " päivä ja viikko on "
        + str(get_week())
        + ". "
        + "Sää Oulussa on nyt "
        + str(current_temp)
        + " °C, "
        + str(current_summary).lower()
        + ". "
        + "Päivän korkein lämpötila on "
        + "%.2f" % round(highest_temp, 2)
        + " °C ja päivän alin lämpötila on "
        + "%.2f" % round(lowest_temp, 2)
        + " °C"
        + ". "
        + "Nimipäivää tänään viettää "
        + get_name_day(1, "https://www.nimipaivat.fi/")
        + "\n\n"
        + "Päivän viisaus on: "
        + get_wisdom()
    )
    return greeting


class GoodMorning:
    job_queue = None
    job = None
    chat_id = None
    current_schedule = "***ajastin ei ole asetettu***"

    def __init__(self, job_queue):
        self.job_queue = job_queue

    """
    Command handlers
    """

    def send_help_good_morning(self, bot):
        bot.send_message(
            chat_id=self.chat_id,
            text="Tiedot:"
            + "\n    -kellonaika: "
            + datetime.datetime.now().strftime("%H:%M")
            + "\n    -ajastin: "
            + self.current_schedule
            + "\n"
            + "\nKomennot:"
            + "\n    init"
            + "\n        -parametrien määrä: 0"
            + "\n        -parameterit: "
            + "\n        -kuvaus: asettaa oikean chat id:n, asettaa kellon "
            "oletusaikaan "
            + "(09:00, ei ota huomioon aikavyöhykkeen ero) ja käynnistää sen."
            + "\n"
            + "\n    set"
            + "\n        -parametrien määrä: 2"
            + "\n        -parametrit: tunti, minuutti"
            + "\n        -kuvaus: asettaa kelloon ajastimen syötetyn ajan "
            "mukaisesti, "
            + 'esimerkiksi: "/huomenta set 22 0" asettaa kelloon ajastimen '
            + "ajalle 22:00 (muista ottaa huomioon aikavyöhykkeen ero)"
            + "\n"
            + "\n    stop"
            + "\n        -parametrien määrä: 0"
            + "\n        -parametrit: "
            + "\n        -kuvaus: pysäyttää ajastimen kokonaan"
            + "\n"
            + "\n    force"
            + "\n        -parametrien määrä: 0"
            + "\n        -parametrit: "
            + "\n        -kuvaus: pakottaa sanomaan Hyvää huomenta",
        )

    def initialize_good_morning(self, bot):
        if self.job is not None:
            self.job.schedule_removal()

        self.job = self.job_queue.run_daily(
            self.say_good_morning, time=time(hour=9, minute=0), context=self.chat_id
        )
        self.current_schedule = "09:00"
        bot.send_message(
            chat_id=self.chat_id,
            text="Chat id ja ajastin asetettu oletus aikaan, eli 09:00.\nKello on tällä hetkellä: "
            + datetime.datetime.now().strftime("%H:%M"),
        )

    def set_good_morning_schedule(self, bot, args):
        try:
            if len(args) != 2:
                raise SyntaxError("Too many or too less arguments for the command")

            hour = int(args[0])
            minute = int(args[1])

            if hour > 24 or hour < 0 or minute < 0 or minute > 60:
                raise SyntaxError(
                    "Hour/minute is invalid (hour neeeds to be between 0 - 24 and minute needs to be "
                    "between 0 - 60"
                )

            if self.job is not None:
                self.job.schedule_removal()

            self.job = self.job_queue.run_daily(
                self.say_good_morning,
                time=time(hour=hour, minute=minute),
                context=self.chat_id,
            )
            self.current_schedule = "{:02d}:{:02d}".format(hour, minute)
            bot.send_message(
                chat_id=self.chat_id,
                text="Huomenta -ajastin on nyt asetettu ajalle: {:02d}:{:02d}".format(
                    hour, minute
                )
                + "\nKello on tällä hetkellä: "
                + datetime.datetime.now().strftime("%H:%M"),
            )
        except (ValueError, SyntaxError):
            bot.send_message(
                chat_id=self.chat_id,
                text="Virheellinen parametri syötetty!\nArgumenttien määrä pitää olla 2 kpl, Parametrien "
                "määrän täytyy olla 2 kpl, molempien argumenttien täytyy olla numeroita ja tuntien "
                "täytyy olla välillä 0 - 24 ja minuuttien täytyy olla välillä 0 - 60, "
                'esimerkiksi:\n"/huomenta_set 9 0".',
            )

    def stop_good_morning_schedule(self, bot):
        if self.job is not None:
            self.job.schedule_removal()
            self.job = None

        if self.chat_id is not None:
            self.chat_id = None

        bot.send_message(
            chat_id=self.chat_id, text="Huomenta ajastin poistettu kokonaan"
        )

    def force_good_morning(self, bot):
        bot.send_message(chat_id=self.chat_id, text=get_greeting())

    def say_good_morning(self, context):
        if self.job is None:
            self.job = context.job
        context.bot.send_message(chat_id=context.job.context, text=get_greeting())

    def handle_command(self, update, context):
        args = context.args
        bot = context.bot
        self.chat_id = update.message.chat_id

        if len(args) < 1:
            self.send_help_good_morning(bot)
            return

        cmd_prefix = args.pop(0).lower()

        if cmd_prefix == "init":
            if len(args) == 0:
                self.initialize_good_morning(bot)
            else:
                bot.send_message(
                    chat_id=self.chat_id,
                    text='init komento ei vaadi parametrejä, esimerkiksi: "/huomenta init"',
                )
        elif cmd_prefix == "set":
            if len(args) == 2:
                self.set_good_morning_schedule(bot, args)
            else:
                bot.send_message(
                    chat_id=self.chat_id,
                    text='set komento vaatii 2 parametriä, esimerkiksi: "huomenta set 22 19"',
                )
        elif cmd_prefix == "stop":
            if len(args) == 0:
                self.stop_good_morning_schedule(bot)
            else:
                bot.send_message(
                    chat_id=self.chat_id,
                    text='stop komento ei vaadi parametrejä, esimerkiksi: "/huomenta stop"',
                )
        elif cmd_prefix == "force":
            if len(args) == 0:
                self.force_good_morning(bot)
            else:
                bot.send_message(
                    chat_id=self.chat_id,
                    text='force komento ei vaadi parametrejä, esimerkiksi: "/huomenta force"',
                )
        else:
            self.send_help_good_morning(bot)
