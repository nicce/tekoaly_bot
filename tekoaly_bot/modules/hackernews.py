# import argparse
from ..conf.conf import logger
from ..utils import CustomArgumentParser
import telegram

import argparse
import requests
import math

# from html import escape
# import re

# import json

HACKERNEWS_API_URL = "https://hacker-news.firebaseio.com/v0"
DEFAULT_AMOUNT_TOPICS_TO_SORT = 30
DEFAULT_AMOUNT_STORIES_TO_SHOW = 5


def get_hacky_stories(amount_stories_to_sort, amount_stories_to_show):

    with requests.Session() as s:
        s.headers.update({"Accept": "application/json, */*"})
        try:
            # Let's try connect into the server and
            # see if it is online.
            resp = s.get(HACKERNEWS_API_URL)
        except ConnectionError as e:
            logger.debug(e)
            logger.debug(resp)
            # if not (resp.status_code == 200):
            print("Error!\nUnable to connect to the server. No news at this time :(")
            return None

        logger.debug("Breaking news!")

        top_ids = s.get(f"{HACKERNEWS_API_URL}/topstories.json").json()[
            :amount_stories_to_sort
        ]

        # best_500_ids = requests.get(
        #     "https://hacker-news.firebaseio.com/v0/beststories.json"
        # ).json()

        logger.debug(f"Top {amount_stories_to_sort} hacky ids acquired.")

        # top_30_ids = top_500_ids[:30]
        top_stories = []
        for story in top_ids:
            single_story = s.get(f"{HACKERNEWS_API_URL}/item/{story}.json").json()
            # logger.debug(json.dumps(top30_single_item, indent=4))
            top_stories.append(single_story)

        logger.debug(f"Top {amount_stories_to_sort} stories acquired by id.")

        top_stories_sorted = sorted(top_stories, key=lambda k: k["score"], reverse=True)
        logger.debug("Stories sorted by score.")
        # true_top_10_stories = top_stories_sorted[:amount_stories_to_show]
    # logger.debug(true_top_10_stories)
    # create_topics(true_top_10_stories)

    # Add ranking value into the story dict object
    return [
        (lambda index, st: st.update({"ranking": index}) or st)(index, st)
        for index, st in enumerate(top_stories_sorted)
    ]


def create_topics(stories):
    logger.debug("Generating response message ofHackernews topics.")
    message = "What Shall I Read Today?  \n\n"
    for story in stories:
        title = story.get("title")
        score = story.get("score")
        url = story.get("url")
        rank = story.get("ranking")
        message += f"**{rank + 1}. [{title.translate({ord(i): ' ' for i in '[]'})}]({url}) Score: {score}**  \n\n"
    return message


class GetHackyNews:
    def __init__(self, bot):
        self.parser = CustomArgumentParser(
            prog="HACKERNEWS",
            description="Top x topikeista kymmennen otettu scoren mukaan HackerNewsistä.",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        self.parser.add_argument(
            "--amount_to_process",
            "-p",
            metavar="{lukumäärä_prosessoida}",
            type=int,
            nargs="?",
            default=DEFAULT_AMOUNT_TOPICS_TO_SORT,
            help="Anna lukumäärä, joka vastaa määrää huippuaiheista, joista kymmenen parasta palautetaan pisteytyksen mukaan. Huomaa, että huippuaiheet eivät määräydy pisteytyksen perusteella.",
        )

        self.parser.add_argument(
            "--amount_to_show",
            "-s",
            default=DEFAULT_AMOUNT_STORIES_TO_SHOW,
            type=int,
            nargs="?",
            help="Lukumäärä näytettävistä aiheista.",
        )
        self.arguments = None

        self.bot = bot
        self.chat_id = None
        self.top_stories = []
        self.top_stories_chunks = []
        self.stories_index = 0
        self.stories_amount_sections = None

    def send_topics(self):
        self.stories_index = 0
        self.stories_amount_sections = math.ceil(
            self.arguments.amount_to_process / self.arguments.amount_to_show
        )
        reply_markup = telegram.InlineKeyboardMarkup(
            [[telegram.InlineKeyboardButton("More topics", callback_data="next")]]
        )
        self.top_stories = get_hacky_stories(
            self.arguments.amount_to_process, self.arguments.amount_to_show
        )
        self.chunk_stories()
        response = create_topics(self.get_stories_chunk())
        logger.debug("Trying to send response about hacky topics.")
        self.bot.send_message(
            chat_id=self.chat_id,
            text=response,
            disable_web_page_preview=True,
            parse_mode="Markdown",
            reply_markup=reply_markup,
        )
        logger.debug("Response send.")

    def chunk_stories(self):
        self.top_stories_chunks = [
            self.top_stories[i : i + self.arguments.amount_to_show]
            for i in range(0, len(self.top_stories), self.arguments.amount_to_show)
        ]
        # return self.top_stories[: self.arguments.amount_to_process * 1]

    def get_stories_chunk(self):
        if self.stories_index < self.stories_amount_sections:
            return self.top_stories_chunks[self.stories_index]
        elif self.stories_index < 0:
            logger.debug(
                f"Stories chunk index too LOW. It is {self.stories_index}. Showing first entries."
            )
            return self.top_stories_chunks[0]
        else:
            logger.debug(
                f"Stories chunk index too BIG. It is {self.stories_index}. Showing last entries."
            )
            return self.top_stories_chunks[self.stories_amount_sections - 1]

    # def newsBrowserQueryHandlerForwarder(bot, update):
    #     # Can't register class method with 'self' parameter as handler
    #     self.newsBrowserQueryHandler(bot, update)

    def gen_reply_markup_buttons(self):
        if self.stories_index == 0:
            reply_markup = telegram.InlineKeyboardMarkup(
                [[telegram.InlineKeyboardButton("More topics", callback_data="next")]]
            )
        elif self.stories_index == self.stories_amount_sections - 1:
            reply_markup = telegram.InlineKeyboardMarkup(
                [
                    [
                        telegram.InlineKeyboardButton(
                            "Previous topics", callback_data="previous"
                        )
                    ]
                ]
            )
        else:
            reply_markup = telegram.InlineKeyboardMarkup(
                [
                    [
                        telegram.InlineKeyboardButton(
                            "Previous", callback_data="previous"
                        ),
                        telegram.InlineKeyboardButton("Next", callback_data="next"),
                    ]
                ]
            )
        return reply_markup

    def newsBrowserQueryHandler(self, update, context):
        logger.debug("Button press detected on Hackynews!")

        if update.callback_query.data == "next":
            self.bot.answerCallbackQuery(
                callback_query_id=update.callback_query.id,
                text="Showing next stories...",
            )
            if self.stories_index < self.stories_amount_sections - 1:
                self.stories_index += 1
            else:
                self.bot.answerCallbackQuery(
                    callback_query_id=update.callback_query.id,
                    text="Unable to show more. Adjust max amount when calling the bot.",
                )
            reply_markup = self.gen_reply_markup_buttons()
            self.bot.editMessageText(
                message_id=update.callback_query.message.message_id,
                chat_id=update.callback_query.message.chat.id,
                text=create_topics(self.get_stories_chunk()),
                reply_markup=reply_markup,
                disable_web_page_preview=True,
                parse_mode="Markdown",
            )
        elif update.callback_query.data == "previous":
            self.bot.answerCallbackQuery(
                callback_query_id=update.callback_query.id,
                text="Showing previous stories...",
            )
            if self.stories_index > 0:
                self.stories_index -= 1
            else:
                self.bot.answerCallbackQuery(
                    callback_query_id=update.callback_query.id,
                    text="Unable to get back. Showing already first entries.",
                )
            reply_markup = self.gen_reply_markup_buttons()
            self.bot.editMessageText(
                message_id=update.callback_query.message.message_id,
                chat_id=update.callback_query.message.chat.id,
                text=create_topics(self.get_stories_chunk()),
                reply_markup=reply_markup,
                disable_web_page_preview=True,
                parse_mode="Markdown",
            )
        else:
            logger.warning("Unknown callback query data.")

    def handle_command(self, update, context):
        self.chat_id = update.message.chat_id

        logger.debug("Starting parsing arguments of HACKYNEWS")
        try:
            self.arguments = self.parser.parse_args(context.args)

        except SystemExit:
            # Ignore system exit on wrong arguments
            while self.parser.argparse_output:
                self.bot.send_message(
                    chat_id=self.chat_id, text=self.parser.argparse_output.pop()
                )
            return

        logger.debug("Arguments parsed successfully.")
        self.send_topics()
        # self.bot.send_chat_action(chat_id=self.chat_id, action=telegram.ChatAction.TYPING)
        # self.send_top10_topics()
