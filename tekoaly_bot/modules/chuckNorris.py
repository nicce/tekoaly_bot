import requests
import argparse
from ..utils import CustomArgumentParser

from ..conf.conf import logger


def get_chuck_norris():
    """
    This method fetches a Chuck Norris joke.
    """
    joke_request = requests.get("https://api.chucknorris.io/jokes/random")
    if 200 == joke_request.status_code:
        logger.debug(f"Chuck Norris API request successful")
        joke = joke_request.json()
        return joke["value"]
    else:
        logger.debug(f"Chuck Norris API request failed")
        return None


class ChuckNorris:
    """
    Class for handling Chuck Norris jokes.
    """

    def __init__(self, bot):
        self.parser = CustomArgumentParser(
            prog="/norris",
            description="Hae Chuck Norris -vitsi",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        self.arguments = None

        self.bot = bot
        self.chat_id = None

    def send_joke(self):
        logger.debug(f"All arguments: {self.arguments}")
        joke = get_chuck_norris()

        self.bot.send_message(chat_id=self.chat_id, text=joke)

    def handle_command(self, update, context):
        self.chat_id = update.message.chat_id

        logger.debug("Starting parsing arguments of Chuck Norris")
        try:
            self.arguments = self.parser.parse_args(context.args)

        except SystemExit:
            # Ignore system exit on wrong arguments
            while self.parser.argparse_output:
                self.bot.send_message(
                    chat_id=self.chat_id, text=self.parser.argparse_output.pop()
                )
            return

        logger.debug("Arguments parsed successfully.")
        self.send_joke()
