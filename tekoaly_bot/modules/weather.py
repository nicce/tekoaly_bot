from ..conf.keys import DARK_SKY_API_KEY, GMAPS_API_KEY
import googlemaps
import requests
import argparse
from ..utils import CustomArgumentParser
from ..conf.conf import logger, DEFAULT_LOCATION_COORDS, DEFAULT_COUNTRY, DEFAULT_CITY
from telegram import Update
from telegram.ext import CallbackContext
gmaps = googlemaps.Client(key=GMAPS_API_KEY)


def get_coordinates_by_name(address=DEFAULT_CITY, country=DEFAULT_COUNTRY):
    """
    Method for acquiring latitude and longitute coordinates by address or name,
    by using Google Maps API.

    Method returns None if given address or name is invalid and nothing found.

    :param address: Address of location, or descriptive name. Default location is Oulu
    :param country: Country of the location. Default is Finland.
    :return: Dictionary object with 'lat' and 'lng' attributes.
    """
    location_data = gmaps.geocode(f"{address}, {country}")
    if location_data:
        location_coords = location_data[0].get("geometry").get("location")
        # address_componenets = location_data[0].get("address_components")
        address = location_data[0].get("formatted_address")
        logger.debug(
            f"Location coords in get_coordinates: {location_coords} and long name for place from coords is: {address}"
        )
        if location_coords and address:
            # long_name = ", ".join([str(component.get("long_name"))
            #                        for component in address_componenets])
            return location_coords, address
        else:
            return None
    else:
        return None


"""
Get weather information
"""


def get_temp(location=None, location_name=None, only_values=True):
    try:
        location_d = (
            f'{location.get("lat")},{location.get("lng")}'
            if location and location_name
            else f'{DEFAULT_LOCATION_COORDS.get("lat")}, {DEFAULT_LOCATION_COORDS.get("lng")}'
        )
        if location and not location_name:
            location_name = gmaps.reverse_geocode(
                (location.get("lat"), location.get("lng"))
            )[0].get("formatted_address")
        else:
            location_name = (
                location_name
                if location_d and location_name
                else gmaps.reverse_geocode(
                    (
                        f'{DEFAULT_LOCATION_COORDS.get("lat")}',
                        f'{DEFAULT_LOCATION_COORDS.get("lng")}',
                    )
                )[0].get("formatted_address")
            )

        logger.debug(
            f'Location coords in "get_temp" are {location_d} and location name is {location_name}'
        )
        # for index, address in enumerate(gmaps.reverse_geocode((lat, lng))):
        # if index == 0:
        # print(address.get("formatted_address"))
        weather_json = requests.get(
            f"https://api.darksky.net/forecast/{DARK_SKY_API_KEY}/{location_d}?exclude=minutely,"
            "hourly,alerts,flags&lang=fi&units=si"
        ).json()
        current_temp = weather_json["currently"]["temperature"]
        highest_temp = weather_json["daily"]["data"][1]["temperatureMax"]
        lowest_temp = weather_json["daily"]["data"][1]["temperatureMin"]
        current_summary = weather_json["currently"]["summary"]
        logger.debug(
            f"Temperatures in get_temp: Current: {current_temp}, Highest: {highest_temp}, Lowest: {lowest_temp}, "
            f"Current Summary: {current_summary}"
        )

        if not only_values:
            return (
                f"Paikka: {location_name}\n"
                f"Lämpötila nyt: {round(current_temp, 1)} °C, "
                + str(current_summary).lower()
                + "\n"
                f"Päivän korkein lämpötila on: {round(highest_temp, 1)} °C\n"
                f"Päivän alin lämpötila on: {round(lowest_temp, 1)} °C"
            )

        else:
            return current_temp, highest_temp, lowest_temp, current_summary
    except Exception as e:
        return "***Weather API connection error! " + e.__str__() + "***"


class GetWeather:
    def __init__(self, bot):
        self.parser = CustomArgumentParser(
            prog="ENNUSTAJA",
            description="Hae haluamasi sijainnin säätiedot nimen, osoitteen tai minkä tahansa kuvaavan "
            "nimen perusteella, ja katsotaan kuinka käy! "
            "Säätiedoista näkyy tämänhetkinen, sekä vuorokauden alin ja ylin lämpötila.",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        self.parser.add_argument(
            "location",
            metavar="{paikka}",
            type=str,
            nargs="*",
            default=DEFAULT_CITY,
            help="Paikannimi, osoite tai mikä tahansa mielivaltainen kuvaava nimi. Mikäli haluat määrittää maan, tee se erillisellä parametrilla.",
        )
        self.parser.add_argument(
            "--maannimi",
            "-m",
            default=DEFAULT_COUNTRY,
            type=str,
            nargs="?",
            help="Anna maan nimi haettavaksi hakukohteeksi.",
        )
        self.arguments = None

        self.bot = bot
        self.chat_id = None

    """
    Command handlers
    """

    def send_weather_state(self):
        logger.debug(f"All arguments: {self.arguments}")
        logger.debug(f"Country is {self.arguments.maannimi}")
        coords = get_coordinates_by_name(
            self.arguments.location, self.arguments.maannimi
        )

        response = (
            get_temp(coords[0], coords[1], only_values=False)
            if coords
            else "Antamasi sijaintitieto ei vastannut yhtään Googlen tiedossa olevaa paikkaa."
        )
        self.bot.send_message(chat_id=self.chat_id, text=response)

    def send_help_weather_info(self):
        logger.debug("Sending help of weather.")
        self.bot.send_message(chat_id=self.chat_id, text=self.parser.format_help())

    # def handle_command(self, bot, update, args):
    def handle_command(self, update: Update, context: CallbackContext):
        self.chat_id = update.message.chat_id

        logger.debug("Starting parsing arguments of ENNUSTAJA")
        try:
            self.arguments = self.parser.parse_args(context.args)

        except SystemExit:
            # Ignore system exit on wrong arguments
            while self.parser.argparse_output:
                self.bot.send_message(
                    chat_id=self.chat_id, text=self.parser.argparse_output.pop()
                )
            return

        logger.debug("Arguments parsed successfully.")
        self.send_weather_state()
